package com.example.fahrtenbuch_schauer;

import java.sql.Date;

public class EntryTbl {
    public static final String TABLE_NAME = "Entry";


    public final static String entry_id = "entry_id";
    public final static String pkw_type = "pkw_type";
    public final static String plate = "plate";
    public final static String reason = "reason";
    public final static String date = "date";
    public final static String time_depature = "time_depature";
    public final static String time_arrival = "time_arrival";
    public final static String km_depature = "km_depature";
    public final static String km_arrival = "km_arrival";
    public final static String km_work = "km_work";
    public final static String km_private = "km_private";
    public final static String from = "from";
    public final static String over = "over";
    public final static String end = "end";
    public static final String [] ALL_COLUMNS = new String[]{entry_id + " AS _id",pkw_type,plate,reason,date,time_depature,time_arrival,km_depature,km_arrival,km_work,km_private,from,over,end};

    public static final String SQL_DROP = "DROP TABLE IF EXISTS "+ TABLE_NAME;
    public static final String SQL_CREATE =
            "CREATE TABLE "+TABLE_NAME+
                    "("+
                    entry_id + " INTEGER PRIMARY KEY,"+
                    pkw_type + " TEXT,"+
                    plate + " TEXT,"+
                    reason + " TEXT,"+
                    date + " DATE,"+
                    time_depature + " TEXT,"+
                    time_arrival + " TEXT,"+
                    km_depature + " TEXT,"+
                    km_arrival + " TEXT,"+
                    km_work + " TEXT,"+
                    km_private + " TEXT,"+
                    from + " TEXT,"+
                    over + " TEXT,"+
                    end + " TEXT,"+
                    ")";
    public static final String STMT_DELETE = "DELETE FROM "+ TABLE_NAME;
    public static final String STMT_INSERT_ =
            "INSERT INTO "+TABLE_NAME+
                    "("+pkw_type
}

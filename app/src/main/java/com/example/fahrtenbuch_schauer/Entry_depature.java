package com.example.fahrtenbuch_schauer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Entry_depature extends AppCompatActivity {
    Spinner pkw_spinner;
    EditText plate_txt;
    EditText reason;
    EditText date;
    EditText depature_time;
    EditText depature_km;
    EditText start;
    String[] bankNames={"Audi","BMW","VW","Opel","Ford"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_depature);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.symbolactionbar);


        //Weiter Arbeiten

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,bankNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pkw_spinner.setAdapter(adapter);
    }

    public Date StringToDate(String string)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");
        try {
            Date date = format.parse(string);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Date StringToTime(String string)
    {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm");
        try {
            Date time = format.parse(string);
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void homescreen(View view)
    {
        pkw_spinner = findViewById(R.id.pkw_spinner);
        plate_txt = findViewById(R.id.plate_txt);
        reason = findViewById(R.id.purpose_txt);
        date = findViewById(R.id.date_txt);
        depature_time = findViewById(R.id.time_txt);
        depature_km = findViewById(R.id.km_txt);
        start = findViewById(R.id.start_txt);
        Date time = StringToTime(depature_time.getText().toString());
        Date new_date = StringToDate(date.getText().toString());
        Fahrtenbuch_object fbo = new Fahrtenbuch_object(pkw_spinner.toString(),plate_txt.getText().toString(),reason.getText().toString(),new_date,time,Integer.parseInt(depature_km.getText().toString()),start.getText().toString());


        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("fsr",fbo);
        startActivity(intent);
    }
}

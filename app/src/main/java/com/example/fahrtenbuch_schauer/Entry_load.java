package com.example.fahrtenbuch_schauer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class Entry_load extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_entry);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.symbolactionbar);
    }
}

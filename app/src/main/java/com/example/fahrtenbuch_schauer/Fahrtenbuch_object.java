package com.example.fahrtenbuch_schauer;

import java.io.Serializable;
import java.util.Date;

public class Fahrtenbuch_object implements Serializable {
    String entry_id;
    String pkw_type;
    String plate;
    String reason;
    Date date;
    Date time_depature;
    Date time_arrival;
    double km_depature;
    double km_arrival;
    double km_work;
    double km_private;
    String from;
    String over;
    String end;

    public Fahrtenbuch_object() {
    }

    public Fahrtenbuch_object(String pkw_type, String plate, String reason, Date date, Date time_depature, double km_depature, String from) {
        this.pkw_type = pkw_type;
        this.plate = plate;
        this.reason = reason;
        this.date = date;
        this.time_depature = time_depature;
        this.km_depature = km_depature;
        this.from = from;
    }

    public String getPkw_type() {
        return pkw_type;
    }

    public void setPkw_type(String pkw_type) {
        this.pkw_type = pkw_type;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime_depature() {
        return time_depature;
    }

    public void setTime_depature(Date time_depature) {
        this.time_depature = time_depature;
    }

    public Date getTime_arrival() {
        return time_arrival;
    }

    public void setTime_arrival(Date time_arrival) {
        this.time_arrival = time_arrival;
    }

    public void setKm_depature(int km_depature) {
        this.km_depature = km_depature;
    }


    public void setKm_arrival(int km_arrival) {
        this.km_arrival = km_arrival;
    }


    public void setKm_work(int km_work) {
        this.km_work = km_work;
    }

    public void setKm_private(int km_private) {
        this.km_private = km_private;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getOver() {
        return over;
    }

    public void setOver(String over) {
        this.over = over;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public double getKm_depature() {
        return km_depature;
    }

    public void setKm_depature(double km_depature) {
        this.km_depature = km_depature;
    }

    public double getKm_arrival() {
        return km_arrival;
    }

    public void setKm_arrival(double km_arrival) {
        this.km_arrival = km_arrival;
    }

    public double getKm_work() {
        return km_work;
    }

    public void setKm_work(double km_work) {
        this.km_work = km_work;
    }

    public double getKm_private() {
        return km_private;
    }

    public void setKm_private(double km_private) {
        this.km_private = km_private;
    }

    public String getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(String entry_id) {
        this.entry_id = entry_id;
    }
}

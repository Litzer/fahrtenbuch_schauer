package com.example.fahrtenbuch_schauer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    Fahrtenbuch_object fbo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        Bundle params = intent.getExtras();
        if(params != null)
        {
            fbo = (Fahrtenbuch_object) params.get("fbo");
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.symbolactionbar);
        if(fbo != null)
        {

        }
    }
    public void new_depature_screen(View v)
    {
        Intent intent = new Intent(this, Entry_depature.class);
        startActivity(intent);
    }

    public void load_edit_screen(View v)
    {
        Intent intent = new Intent(this, Entry_arrival.class);
        startActivity(intent);
    }
}